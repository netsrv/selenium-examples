# Selenium Examples
This project demonstrates basic Selenium usage both standalone and via a grid.

The code is largely from Selenium's getting started guide.

## Instructions
First build it...
`mvn clean compile`

Then to run locally...

`mvn exec:java -Dexec.mainClass="com.netsrvconsulting.demo.selenium.Example"`

Or to run against a Selenium grid (change the URL of the hub to be yours) ...

`mvn exec:java -Dexec.mainClass="com.netsrvconsulting.demo.selenium.GridExample" -Dexec.args="http://myseleniumhub:4444/wd/hub"`

Note: your grid will need Firefox support.

## License and Authors
Distributed under the terms of [The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)

Authors:
Colin Woodcock <cwoodcock at netsrv-consulting.com>

