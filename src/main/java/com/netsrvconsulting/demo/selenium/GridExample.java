/*
* Copyright 2013 NetSrv Consulting Ltd.
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.netsrvconsulting.demo.selenium;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

//
// See https://code.google.com/p/selenium/wiki/GettingStarted
//
public class GridExample {
	public static void main(String[] args) throws Exception {
		if (args.length == 0) throw new Exception("Expected the hub URL to use as an argument");

		// Specify the desired test environment
		//DesiredCapabilities capability = DesiredCapabilities.htmlUnit();
		DesiredCapabilities capability = DesiredCapabilities.firefox();
		
		URL hub = new URL(args[0]);
		
		// Get the driver passing in the hub to use and the capabilities needed
		WebDriver driver = new RemoteWebDriver(hub, capability);

		// The rest is the same as in Example...
		
		// And now use this to visit Google
		driver.get("http://www.google.com");

		// Find the text input element by its name
		WebElement element = driver.findElement(By.name("q"));

		// Enter something to search for
		element.sendKeys("Cheese!");

		// Now submit the form. WebDriver will find the form for us from the
		// element
		element.submit();

		// Check the title of the page
		System.out.println("Page title is: " + driver.getTitle());
	}
}
